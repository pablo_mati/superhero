package com.hiberus.superhero.repository;

import com.hiberus.superhero.domain.entity.Hero;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ISuperHeroRepository extends JpaRepository<Hero, Long> {

    List<Hero> findByNameContainingIgnoreCase(String name);


}

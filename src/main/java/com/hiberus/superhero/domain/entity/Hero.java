package com.hiberus.superhero.domain.entity;

import lombok.*;

import javax.persistence.*;

@Data
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "superheroes")
public class Hero {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Long id;

    @Column
    String name;

    @Column
    private String power;

}

package com.hiberus.superhero.controller;

import com.hiberus.superhero.domain.entity.Hero;
import com.hiberus.superhero.exception.HeroNotFoundException;
import com.hiberus.superhero.service.ISuperHeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/superhero", produces = MediaType.APPLICATION_JSON_VALUE)
public class SuperHeroControllerImpl implements ISuperHeroController {

    private final ISuperHeroService iSuperHeroService;

    @Autowired
    public SuperHeroControllerImpl(ISuperHeroService iSuperHeroService) {
        this.iSuperHeroService = iSuperHeroService;
    }

    @Override
    @GetMapping("/all")
    public ResponseEntity retrieveAllHero() {
        return ResponseEntity.ok(iSuperHeroService.retrieveAllHero());
    }

    @Override
    @GetMapping("/{id}")
    public ResponseEntity retrieveHeroById(@PathVariable("id") Long id) throws HeroNotFoundException {

        return ResponseEntity.ok(iSuperHeroService.retrieveHeroById(id));
    }

    @Override
    @GetMapping("/all/{name}")
    public ResponseEntity retrieveHeroesByName(@PathVariable("name") String name) {
        return ResponseEntity.ok(iSuperHeroService.retrieveHeroesByName(name));
    }

    @Override
    @PutMapping("/{id}")
    public ResponseEntity updateHero(@PathVariable("id") Long id, @RequestBody Hero hero) throws HeroNotFoundException {
        return ResponseEntity.ok(iSuperHeroService.updateHero(id, hero));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteHero(@PathVariable("id") Long id) throws HeroNotFoundException {
        iSuperHeroService.deleteHero(id);
        return ResponseEntity.accepted().build();
    }
}

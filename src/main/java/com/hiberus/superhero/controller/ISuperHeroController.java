package com.hiberus.superhero.controller;

import com.hiberus.superhero.domain.entity.Hero;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ISuperHeroController {

    @ApiOperation("Returns all superheroes stored in the database")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Data found", response = Hero[].class)
    })
    ResponseEntity retrieveAllHero();

    @ApiOperation(value = "Searchs for a hero with the given id in the database")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Hero found", response = Hero.class),
            @ApiResponse(code = 404, message = "Hero with Id 1 not found")
    })
    ResponseEntity retrieveHeroById(Long id) ;

    ResponseEntity retrieveHeroesByName(String name);

    @ApiOperation(value = "Update a hero with the given id in the database")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Hero updated", response = Hero.class),
            @ApiResponse(code = 404, message = "Hero with Id 1 not found")
    })
    ResponseEntity updateHero(Long id, Hero hero);

    @ApiOperation(value = "Delete a hero with the given id in the database")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Hero deleted"),
            @ApiResponse(code = 404, message = "Hero with Id 1 not found")
    })
    ResponseEntity deleteHero(Long id);
}

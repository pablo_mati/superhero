package com.hiberus.superhero.exception;

public class HeroNotFoundException extends RuntimeException {

    public HeroNotFoundException(Long id) {

        super(String.format("Hero with Id %d not found", id));
    }
}


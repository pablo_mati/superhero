package com.hiberus.superhero.service;

import com.hiberus.superhero.domain.entity.Hero;
import com.hiberus.superhero.exception.HeroNotFoundException;

import java.util.List;

public interface ISuperHeroService {

    List<Hero> retrieveAllHero();

    Hero retrieveHeroById(Long id);

    List<Hero> retrieveHeroesByName(String name);

    Hero updateHero(Long id, Hero hero) throws HeroNotFoundException;

    void deleteHero(Long id) throws HeroNotFoundException;


}

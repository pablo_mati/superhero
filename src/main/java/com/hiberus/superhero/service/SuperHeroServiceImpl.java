package com.hiberus.superhero.service;

import com.hiberus.superhero.domain.entity.Hero;
import com.hiberus.superhero.exception.HeroNotFoundException;
import com.hiberus.superhero.repository.ISuperHeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SuperHeroServiceImpl implements ISuperHeroService {

    private final ISuperHeroRepository iSuperHeroRepository;

    @Autowired
    public SuperHeroServiceImpl(final ISuperHeroRepository iSuperHeroRepository) {

        this.iSuperHeroRepository = iSuperHeroRepository;
    }

    /**
     * Method that returns the list of superheroes
     *
     * @return List<Hero> all heroes.
     */
    @Override
    @Cacheable(cacheNames = "superheroes")
    public List<Hero> retrieveAllHero() {
        return iSuperHeroRepository.findAll();
    }

    /**
     * Method that searches hero by id
     *
     * @param id
     * @return Hero
     * @throws HeroNotFoundException Hero not found
     */
    @Override
    @Cacheable(value = "superheroes", unless = "#result != null")
    public Hero retrieveHeroById(Long id) throws HeroNotFoundException {
        return iSuperHeroRepository.findById(id).orElseThrow(() -> new HeroNotFoundException(id));
    }

    /**
     * Method that searches heroes by name
     *
     * @param name
     * @return List<Hero>
     */
    @Override
    public List<Hero> retrieveHeroesByName(String name) {
        return iSuperHeroRepository.findByNameContainingIgnoreCase(name);
    }

    /**
     * Method that updates hero by id
     *
     * @param id
     * @param hero
     * @return Hero updated
     * @throws HeroNotFoundException Hero not found
     */
    @Override
    @CacheEvict(value = "superheroes", allEntries = true)
    public Hero updateHero(Long id, Hero hero) throws HeroNotFoundException {
        return iSuperHeroRepository.findById(id)
                .map(x -> {
                    x.setName(hero.getName());
                    x.setPower(hero.getPower());
                    return iSuperHeroRepository.save(x);
                })
                .orElseThrow(() -> new HeroNotFoundException(id));

    }

    /**
     * Method that removes hero by id
     *
     * @param id
     * @throws HeroNotFoundException Hero not found
     */
    @Override
    @CacheEvict(value = "superheroes", allEntries = true)
    public void deleteHero(Long id) throws HeroNotFoundException {
        iSuperHeroRepository.findById(id)
                .map(x -> {
                    iSuperHeroRepository.delete(x);
                    return "Hero deleted";
                })
                .orElseThrow(() -> new HeroNotFoundException(id));
    }


}

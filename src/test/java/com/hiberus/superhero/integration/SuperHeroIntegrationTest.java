package com.hiberus.superhero.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hiberus.superhero.SuperheroApplication;
import com.hiberus.superhero.domain.entity.Hero;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = SuperheroApplication.class
)
public class SuperHeroIntegrationTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    public void testGetSuperHeroById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/superhero/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Spiderman"));
    }

    @Test
    public void testGetAllSuperHero() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/superhero/all")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[3].name").value("Iron Man"));
    }

    @Test
    public void testUpdateSuperHero() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .put("/superhero/{id}", 2)
                .content(asJsonString(new Hero(2l, "Superman", "Fly")))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Superman"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.power").value("Fly"));

    }

    @Test
    public void deleteSuperHero() throws Exception
    {
        mockMvc.perform( MockMvcRequestBuilders
                .delete("/superhero/{id}", 1) )
                .andExpect(MockMvcResultMatchers.status().isAccepted());
    }

    private static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}


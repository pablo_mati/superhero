package com.hiberus.superhero.controller;

import com.hiberus.superhero.domain.entity.Hero;
import com.hiberus.superhero.service.ISuperHeroService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SuperheroControllerTest {

    @Mock
    private ISuperHeroService iSuperHeroService;

    @InjectMocks
    private SuperHeroControllerImpl iSuperHeroController;


    @Test
    public void testRetrieveByIdNotFound() {
        when(iSuperHeroService.retrieveHeroById(1L)).thenReturn(Hero.builder()
                .id(1L)
                .name("Superman")
                .power("Fly")
                .build());
        assertThat(iSuperHeroController.retrieveHeroById(1L).getStatusCode()).isEqualByComparingTo(HttpStatus.OK);


        assertThat(iSuperHeroController.retrieveHeroById(1L)).satisfies(r -> {
            assertThat(r.getStatusCode()).isEqualTo(HttpStatus.OK);
            assertThat(r.getBody()).isEqualTo(Hero.builder()
                    .id(1L)
                    .name("Superman")
                    .power("Fly")
                    .build());
        });
    }

    @Test
    public void testGetAllHeroes() {
        List<Hero> heroes = generateHeroes();
        when(iSuperHeroService.retrieveAllHero()).thenReturn(heroes);
        ResponseEntity<String> httpResponse = iSuperHeroController.retrieveAllHero();

        Assert.assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);
        Assert.assertEquals(heroes, httpResponse.getBody());
    }

    @Test
    public void testRetrieveAllShows() {
        when(iSuperHeroService.retrieveAllHero()).thenReturn(Arrays.asList(Hero.builder()
                .id(1L)
                .name("Superman")
                .power("Fly")
                .build()));

        assertThat(iSuperHeroController.retrieveAllHero()).satisfies(r -> {
            assertThat(r.getStatusCode()).isEqualTo(HttpStatus.OK);
            assertThat(r.getBody()).isNotNull();
        });
    }


    private List<Hero> generateHeroes() {
        List<Hero> heroes = new ArrayList<>();

        Hero hero = new Hero(1l, "Captain 1", "Power 1");
        heroes.add(hero);

        hero = new Hero(2l, "Captain 2", "Power 2");
        heroes.add(hero);

        hero = new Hero(3l, "Captain 3", "Power 3");
        heroes.add(hero);

        return heroes;
    }

}

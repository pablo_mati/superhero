# For Java 8
# FROM openjdk:8-jdk-alpine

# For Java 11
FROM adoptopenjdk/openjdk11:alpine-jre
# Maven build -> finalName
ARG JAR_FILE=target/*.jar
# cp target/superhero.jar /app.jar
COPY ${JAR_FILE} app.jar
# java -jar /opt/app/superhero.jar
ENTRYPOINT ["java","-jar","/app.jar"]